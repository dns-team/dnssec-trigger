Source: dnssec-trigger
Section: net
Priority: optional
Maintainer: dnssec-trigger packagers <dnssec-trigger@packages.debian.org>
Uploaders: Ondřej Surý <ondrej@debian.org>,
	   Diane Trout <diane@ghic.org>
Build-Depends: debhelper (>= 11),
	       dh-python,
               libcmocka-dev,
	       libssl-dev (>= 1.1.0),
	       libgtk2.0-dev,
	       libldns-dev (>= 1.6.14-1~),
	       lsb-base,
	       python3,
               sensible-utils
Standards-Version: 4.3.0
Homepage: https://www.nlnetlabs.nl/projects/dnssec-trigger/
Vcs-Git: https://salsa.debian.org/dns-team/dnssec-trigger.git
Vcs-Browser: https://salsa.debian.org/dns-team/dnssec-trigger

Package: dnssec-trigger
Architecture: any
Depends: ${shlibs:Depends},
	 ${misc:Depends},
	 ${python3:Depends},
	 python3,
	 python3-gi,
	 python3-lockfile,
	 gir1.2-nm-1.0 (>= 1.2) [linux-any],
         sensible-utils,
	 unbound
Recommends: e2fsprogs
Conflicts: resolvconf
Description: reconfiguration tool to make DNSSEC work
 Dnssec-trigger reconfigures the local unbound DNS server. This unbound
 DNS server performs DNSSEC validation, but dnssec-trigger will signal
 it to use the DHCP obtained forwarders if possible, and fallback to
 doing its own AUTH queries if that fails, and if that fails prompt the
 user via dnssec-trigger-applet the option to go with insecure DNS
 only.
